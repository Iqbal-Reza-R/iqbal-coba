@extends('master')

@section('title','Tickets')


@section('content')
<div id="content">
    <div class="container">
        <div class="col-10">
            <div class="row">

                <form id="tambah" action="/kategori/store" method="POST">
                    @csrf
                    <div class="form-group">
                        <h3><label>Masukan Type_Id</label></h3>
                        <input type="text" name="tambahjenis" class="form-control" placeholder="Masukan Type Id...">
                    </div>
                    <button type="submit" class="tambah btn btn-primary">Tambahkan</button>
                    <h4 id="pesan"></h4>
                </form>
            </div><br>
            <div class="row">
                <form id="tambahfile" action="" method="" enctype="multipart/form-data">
                    @csrf
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <button type="submit" class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" name="inputFile" class="custom-file-input" id="inputGroupFile01"
                                aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01">Choose File</label>
                        </div>
                    </div>
                </form>
            </div>
                <div class="progress">
                    <div class="progress-bar progress-bar-striped bg-success" role="progressbar" display="none" style="width: 0%"></div>
                </div>
        </div>
    </div>
</div>


@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script defer src="js/axios.js"></script>


@endpush
