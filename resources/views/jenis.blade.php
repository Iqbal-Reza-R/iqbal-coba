@extends('master')

@section('title','Jenis')



@section('content')
<div id="content">
    <div class="container">
        <div class="col-10">

            <form id="tambah" action="/jenis/tambah" method="POST">
                @csrf
                <div class="form-group">
                    <h3><label>Masukan Jenis Laporan Baru</label></h3>
                    <input type="text" name="tambahjenis" class="form-control"
                        placeholder="Masukan Jenis Laporan Baru..." disabled>
                </div>
                <button type="submit" class="tambah btn btn-primary">Tambahkan</button>
                <h4 id="pesan"></h4>
            </form>

        </div>
        <div class="col-10">
            <div class="row">
                <h1 class="mt-3"> Jenis Laporan </h1></div>
                <div class="row">

                <table class="table display" id="jenis-tabel">
                    <thead>
                        <tr>
                            <th >#</th>
                            <th >Nama</th>
                            <th >Ticket</th>
                            <th >Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        {{-- @foreach ($jenis as $jns)

                        <tr>
                            <td scope="row">{{$loop->iteration}}</td>
                            <td class="namadata" data-nama="{{ $jns->nama }}">{{ $jns->nama }}</td>
                            <td>
                                <a href="#" class="edit badge badge-success"
                                    data-id={{ $jns->id }}> EDIT </a>
                                    <a href="/jenis/hapus/{{ $jns->id }}" class="delete badge badge-danger"
                                        onclick="return confirm('Hapus Data??');"> HAPUS </a>
                            </td>
                        </tr>
                        @endforeach --}}
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>


<!-- Modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ubah Jenis Laporan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="" method="POST" id="formModal">
                @csrf
                <div class="modal-body">

                    <div class="form-group">
                        <h3><label>Ganti Nama Laporan</label></h3>
                        <input type="text" name="ubahjenis" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Ubah</button>
                </div>
            </form>
        </div>
    </div>
</div>



@endsection


@push('scripts')
{{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="js/coba.js"></script> --}}

<script src="{{ asset('js/coba.js') }}"></script>

@endpush
