@extends('master')

@section('title','Ubah Data')



@section('content')

<form action="{{url('/jenis/ubah', $jns['id'])}}" method="POST">
        @csrf
    <div class="modal-body">

            <div class="form-group" >
                    <h3><label>Ganti Nama Laporan</label></h3>
                    <input type="text"  name="ubahjenis" class="form-control"  value= {{$jns->nama}}>
            </div>   
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary">Ubah</button>
    </div>
</form>

@stop 