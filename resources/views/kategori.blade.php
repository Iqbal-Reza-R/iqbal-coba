@extends('master')

@section('title','Kategori')



@section('content')
<div id="content">
    <div class="container">
        <div class="col-10">
            <div class="row">
                <h1 class="mt-3"> Ini Halaman kategori </h1>
            </div>
            <div class="dot"></div>
                <div class="row 2">
                    <br><br>
                    <select id="parentKategori">
                        <option value="">-- Pilih --</option>
                        {{-- @foreach($rows as $row)
                            @if($row->parent == 0){
                                <option selected="select" value={{ $row->id }}>{{ $row->nama }}</option>
                            }
                            @endif
                        @endforeach --}}
                    </select>
                    <select id="childKategori">
                        <option value="">--Tidak ada pilihan--</option>
                        {{-- @foreach($rows as $due)
                            @if($due->parent == $row){
                                <option>{{ $due->nama }}</option>
                            }
                            @endif
                        @endforeach --}}
                    </select>
                </div>
            </div>
        </div>
    </div>

@endsection


@push('scripts')
<script src="{{ asset('js/kategori.js') }}"></script>
@endpush
