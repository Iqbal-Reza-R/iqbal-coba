
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;" >
    <div class="container">

        <a class="navbar-brand">All Tickets This Day</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="navbar-nav">
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">

            <a class="nav-item nav-link" href="{{ url('/') }}"> Tickets </a>
            <a class="nav-item nav-link" href="{{ url('/jenis') }}"> Jenis </a>
            <a class="nav-item nav-link" href="{{ url('/kategori') }}"> Kategori </a>

        </div>
        </div>
    </div>
</nav>
