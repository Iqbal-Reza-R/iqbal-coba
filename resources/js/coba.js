import swal from 'sweetalert2'

$(document).ready(function() {
    skeldatatable();
    $("input[name=tambahjenis]").attr('disabled', false)

    // //tampilan tabel
    // $.ajax({
    //     method: "GET",
    //     url: "/jenis/muncul"
    // }).done(function(responses) {
    //     tampildatatable(responses);
    //     $("#jenis-tabel").DataTable();
    // })

    //coba onclick
    $("input[name=tambahjenis]").click(function() {
        $("#content").css("background-color", "white");
    });

    $("input[name=tambahjenis]").blur(function() {
        $("#content").css("background-color", "white");
    });

    //token csrf
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    //tambah data
    $("#tambah").submit(function(event) {
        event.preventDefault();
        $(".tambah").attr("disabled",
        "disabled");

        $.ajax({
            method: "POST",
            url: "/jenis/tambah",
            data: $("#tambah").serialize()
        })
            .done(function(data) {
                // $("#pesan").append(data.pesan);
                $("input[name=tambahjenis]").val("");
                swal.fire("Selamat", "Data berhasil ditambahkan", "success")

            })
            .always(function() {
                setTimeout(function() {
                    // $("#pesan").empty();
                    $(".tambah").attr("disabled", false);
                }, 2000);
                $('#jenis-tabel').DataTable().ajax.reload()

            }).fail(function(responses) {
                console.log(Object.keys(responses.responseJSON.errors))
                $('#notify').empty()
                $(`#tambah *[name=${Object.keys(responses.responseJSON.errors)}]`).parent('div').append(`<div class="alert alert-danger notif">${responses.responseJSON.errors.tambahjenis}</div>`)
                swal.fire("Data Salah", `${responses.responseJSON.errors.tambahjenis}`, "error")
            });
    });

    //hapus data
    // $(document).on("click", "#jenis-tabel a.delete", function(event) {
    //    let urldelete = $(event.target).parent.sibilings
    //     $.ajax({
    //         url:/jenis/hapus/{id}
    //     })

    //menentukan action dan method dari form modal
    $(document).on("click", "#jenis-tabel a.edit", function(asd) {

        // console.log(asd)
        //nentuin kolom keberapa
        let index = $(asd.target).parents('tr').index()
        //narik data dari data tabel berdasarkan kolom ke berapa
        let record = $('#jenis-tabel').DataTable().rows(index).data()

        // let kucing = $(event.target)
        //     .parent()
        //     .siblings(".namadata")
        //     .data("nama");
        let kucing2 = $(asd.target).data("id");
        // // console.log(kucing)
        $("input[name=ubahjenis]").val(record[0].nama)
        $("#formModal").attr("action", "/jenis/ubah/" + kucing2);
        $("#exampleModal").modal("show");
    });

    //The Real Killer
    $(document).on("click", "#jenis-tabel a.realdelete", function(event) {
        let kucing2 = $(event.target).data("id")
        event.preventDefault()
        $.ajax({
            url:'/jenis/realdelete/'+kucing2,
            method:'delete'
        }).done(function() {
            $('#jenis-tabel').DataTable().ajax.reload()
        })
    })


    //RESTORE DATA
    $(document).on("click", "#jenis-tabel a.restore", function(event) {
        let kucing2 = $(event.target).data("id")
        event.preventDefault()
        $.ajax({
            url:'/jenis/restore/'+kucing2,
            method:'PUT'
        }).done(function() {
            $('#jenis-tabel').DataTable().ajax.reload()
        })
    })

    //SoftDelete data
    $(document).on("click", "#jenis-tabel a.delete", function(event) {
        event.preventDefault();
        let kucing2 = $(event.target).data("id")
         // SweetAlert
         const swalWithBootstrapButtons = swal.mixin({
            customClass: {
              confirmButton: 'btn btn-danger',
              cancelButton: 'btn btn-success'
            },
            buttonsStyling: false,
          })

          swalWithBootstrapButtons.fire({
            title: 'Apakah anda yakin??',
            text: 'Data yang telah anda hapus tidak dapat dikembalikan',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Hapus Data',
            cancelButtonText: 'Batal',
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
                swalWithBootstrapButtons.fire(
                    'Terhapus',
                    'Data yang anda pilih telah terhapus',
                    'success'
                )
                $.ajax({
                    Method: 'GET',
                    url: '/jenis/hapus/'+kucing2
                }).done(function(data) {
                    $('#jenis-tabel').DataTable().ajax.reload()
                    // $("#pesan").append(data.pesan);
                // }).always(function() {
                //     // setTimeout(function() {
                //     //     $("#pesan").empty();
                //     // }, 2000);

                //     swal.fire("OK", "Data berhasil dihapus", "error")
                })
            // } else if (
            //   // Read more about handling dismissals
            //   result.dismiss === swal.DismissReason.cancel
            // ) {
            //   swalWithBootstrapButtons.fire(
            //     'Hapus gagal',
            //     'Data anda tetap ada',
            //     'success'
            //   )
            }
          })
        //   batas
        // $.ajax({
        //     Method: 'GET',
        //     url: '/jenis/hapus/'+kucing2
        // }).done(function(data) {
        //     // $("#pesan").append(data.pesan);
        //     $('#jenis-tabel').DataTable().ajax.reload()
        // }).always(function() {
        //     // setTimeout(function() {
        //     //     $("#pesan").empty();
        //     // }, 2000);

        //     swal.fire("OK", "Data berhasil dihapus", "error")

        // })

        // const swalWithBootstrapButtons.fire ({
        //     title: 'Apakah anda yakin??',
        //     text: 'Data yang telah anda hapus tidak dapat dikembalikan',
        //     type: 'warning',
        //     showCancelButton: true,
        //     confirmButtonText: 'Hapus Data',
        //     cancelButtonText: 'Batal',
        //     reverseButtons: false
        // }).then((result) => {
        //     if (result.value) {

        //       swalWithBootstrapButtons.fire(
        //         'Terhapus',
        //         'Data yang anda piluh telah terhapus',
        //         'eror'
        //       )
        //     } else if (
        //       // Read more about handling dismissals
        //       result.dismiss === swal.DismissReason.cancel
        //     ) {
        //       swalWithBootstrapButtons.fire(
        //         'Hapus gagal',
        //         'Data anda tetap ada',
        //         'success'
        //       )
        //     }
        //   })
          //batas

    })

    //Update data dari modal
    $("#formModal").submit(function(event) {
        event.preventDefault();

        $.ajax({
            method: $("#formModal").attr("method"),
            url: $("#formModal").attr("action"),
            data: $("#formModal").serialize()
        })
            .done(function(data) {
                // $("#pesan").append(data.pesan);
                $('#jenis-tabel').DataTable().ajax
                .reload()


            })
            .always(function() {
                setTimeout(function() {
                    $("#exampleModal").modal("hide");
                    swal.fire("Selamat", "Data berhasil diubah", "success")
                }, 100);
                // setTimeout(function() {
                //     $("#pesan").empty();
                // }, 2000);
                // $.ajax({
                //     method: "GET",
                //     url: "/jenis/muncul"
                // }).done(function(responses) {
                //     tampildatatable(responses);
                // }).always(function() {
                //     skeldatatable();
                // });


            });
    });
});




    // let tampildatatable = function(responses) {
    //     $.each(responses, function(index, response) {
    //         $("#jenis-tabel tbody").append(
    //             `<tr>
    //                 <td>${index + 1}</td>
    //                 <td class="namadata" data-nama=${response.nama}>${
    //                 response.nama
    //             }</td>
    //                 <td>
    //                     <a href="#" class="edit badge badge-success" data-id="${
    //                         response.id
    //                     }"> EDIT </a>
    //                     <a href="/jenis/hapus/${
    //                         response.id
    //                     }" class="delete badge badge-danger" onclick="return confirm('Hapus Data??');"> HAPUS </a>
    //                 </td>
    //             </tr>`
    //         );
    //     });
    // }



    //DataTable
    let skeldatatable = function (){
        $('#jenis-tabel').DataTable({
            serverSide: true,
            processing: true,
            // "pageLength": 10,
            ajax: {
                method: "GET",
                url: "/jenis/muncul"
            },
            columns:[
                {
                    data: 'no'
                },
                {
                    data: 'nama'
                },
                {
                    data: 'tickets_count'
                },
                {
                    data: 'id'
                },
            ],
            columnDefs: [
                {
                    "targets": 3,
                    "orderable": false,
                    "render": function ( data, type, row, meta ) {
                        return `
                            <a href="#" class="edit badge badge-success" data-id="${ data }"> EDIT </a>
                            <a href="#" data-id="${ data }" class="delete badge badge-primary" > HAPUS </a>
                            <a href="#" data-id="${ data }" class="restore badge badge-warning" > RESTORE </a>
                            <a href="#" data-id="${ data }" class="realdelete badge badge-danger" > HAPUS!!! </a>
                        `
                    }
                }
                // {
                //     "targets": 2,
                //     "orderable": false,
                //     "render": function ( data, type, row, meta ) {
                //         return `
                //              0
                //         `
                //     }
                // }
            ],

            order:[[ 0, 'dsc']]

        })



        //Searching
        $('input[type=search]').keyup(function() {
            $('#jenis-tabel').DataTable().ajax.reload()
        })
    }



        // // SweetAlert
        // const swalWithBootstrapButtons = swal.mixin({
        //     customClass: {
        //       confirmButton: 'btn btn-success',
        //       cancelButton: 'btn btn-danger'
        //     },
        //     buttonsStyling: false,
        //   })

        //   swalWithBootstrapButtons.fire({
        //     title: 'Apakah anda yakin??',
        //     text: 'Data yang telah anda hapus tidak dapat dikembalikan',
        //     type: 'warning',
        //     showCancelButton: true,
        //     confirmButtonText: 'Hapus Data',
        //     cancelButtonText: 'Batal',
        //     reverseButtons: true
        //   }).then((result) => {
        //     if (result.value) {
        //       swalWithBootstrapButtons.fire(
        //         'Terhapus',
        //         'Data yang anda piluh telah terhapus',
        //         'eror'
        //       )
        //     } else if (
        //       // Read more about handling dismissals
        //       result.dismiss === swal.DismissReason.cancel
        //     ) {
        //       swalWithBootstrapButtons.fire(
        //         'Hapus gagal',
        //         'Data anda tetap ada',
        //         'success'
        //       )
        //     }
        //   })
        // //   batas



