import axios from 'axios'
axios.defaults.headers.common['X-CSRF-TOKEN'] = document.head.querySelector('meta[name="csrf-token"]')

$(document).ready(function () {

    $("#tambah").submit(function (event) {
        event.preventDefault();
        $(".tambah").attr("disabled", "disabled");

        $.ajax({
                method: "POST",
                url: "/ticket/store",
                data: $("#tambah").serialize()
            })
            .done(function () {
                // $("#pesan").append(data.pesan);
                $("input[name=tambahjenis]").val("");
                swal.fire("Selamat", "Data berhasil ditambahkan", "success")

            })
            .always(function (oie) {
                setTimeout(function () {
                    // $("#pesan").empty();
                    $(".tambah").attr("disabled", false);
                }, 2000);
                $('#jenis-tabel').DataTable().ajax.reload()

            });
    });

    // $("#tambahfile").submit(function (event) {
    //     event.preventDefault();
    //     let formdata = new FormData(document.getElementById('tambahfile'))
    //     $.ajax({
    //             method: "POST",
    //             url: "/ticket/file",
    //             data: formdata,
    //             processData: false,
    //             contentType: false
    //         })
    //         .done(function (data) {
    //             $("input[name=inputFile]").val("");
    //             swal.fire("Selamat", "Data berhasil ditambahkan", "success")

    //         })
    //         .fail(function () {
    //             swal.fire("Error", "Data gagal diinput", "error")
    //         })
    // });

    console.log(axios)
    $("#tambahfile").submit(function (event) {
        event.preventDefault();
        let formdata = new FormData(document.getElementById('tambahfile'))
        axios({
            method: "post",
            url: "/ticket/file",
            data: formdata,
            onUploadProgress: function (progressEvent) {
                let {loaded, total} = progressEvent
                let persenan = (loaded / total) * 100
                $('.progress-bar').show()
                $('.progress-bar').css('width' , `${persenan}%`)
              }
        }).then(function(){
            swal.fire("Selamat", "Data berhasil ditambahkan", "success")
            $('.progress-bar').css('width' , `0%`)
        }).catch(function() {
            swal.fire("Error", "Data gagal diinput", "error")
        })

    })

})
