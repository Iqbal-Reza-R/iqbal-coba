<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@ticket');

//Route Ticket
Route::post('/ticket/store', 'TicketController@store');
Route::post('/ticket/file', 'TicketController@file');
Route::get('/ticket' , 'TicketController@index');
//Route Jenis
Route::get('/jenis', 'JenisController@index');
Route::get('/jenis/muncul', 'JenisController@coba');
Route::get('/jenis/datatable', 'JenisController@json');
Route::post('/jenis/tambah', 'JenisController@store');
Route::post('/jenis/ubah/{id}', 'JenisController@update');
Route::get('/jenis/hapus/{id}','JenisController@destroy');
Route::put('/jenis/restore/{id}', 'JenisController@restore');
Route::delete('/jenis/realdelete/{id}', 'JenisController@realdelete');


Route::get('/kategori','PagesController@kategori');
Route::get('/kategori/child','PagesController@child');
Route::get('/kategori/parent','PagesController@parent');
Route::get('/kategori/nel','PagesController@nel');
Route::get('/kategori/hmt','PagesController@hmt');
Route::get('/kategori/mtm','PagesController@mtm');
Route::get('/kategori/accessor','PagesController@accessor');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
