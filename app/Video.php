<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    public $timestamps = false;

    public function roles(){
        return $this->belongsToMany('App\Role', 'role_video', 'role_id', 'video_id');
    }
}
