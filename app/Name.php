<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Name extends Model
{
    public $timestamps = false;
    protected $appends = ['NamaId'];

    public function getNamaIdAttribute(){
        return ucfirst("{$this->nama} {$this->type_id}");
    }

    public function prices(){
        return $this->hasMany('App\Price');
    }

    public function type(){
        return $this->belongsTo('App\Type');
    }


}
