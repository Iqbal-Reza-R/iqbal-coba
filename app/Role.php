<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $timestamps = false;

    public function videos(){
        return $this->belongsToMany('App\Video', 'role_video', 'role_id', 'video_id');
    }
}
