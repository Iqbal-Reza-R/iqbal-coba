<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\softDeletes;

class Type extends Model
{
    public $timestamps = false;
    use SoftDeletes;

    //accesor
    public function getNamaAttribute($value){
        return ucfirst($value);
    }
    //

    //mutator
    public function setNamaAttribute($value){
        $this->attributes['Nama']=ucwords($value);
    }

    public function tickets(){
        return $this->hasMany('App\Ticket');
    }

    public function recursive(){

        return $this->hasOne('App\Type', 'parent', 'id');
    }
    public function superrecursive(){

        return $this->recursive()
                    ->with('superrecursive');
    }

    public function names(){
        return $this->hasMany('App\Name');
    }

    public function prices(){
        return $this->hasManyThrough('App\Price','App\Name');
    }




}
