<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public $timestamps = false;

    public function type(){
        return $this->belongsTo('App\Type');
    }
}
