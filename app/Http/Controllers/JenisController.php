<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Type;
use App\Http\Requests\StoreBlogPost;
use Carbon\Carbon;

class JenisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $jenis = DB::table('types')->get();
        // $jenis = Type::withCount('tickets')->get();

        // foreach ($jenis as $jns){
        //     $data = count($jns->ticket);
        // }
        // dd($jenis);

        // // dd($jenis);
        // $jenis2 = $jenis;
        // $jenis2->nama = "kucing";
        // dd($jenis);
        return view('jenis');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (StoreBlogPost $request)
    {
        //validasi inputan
        // $validateData = $request->validate(['tambahjenis'=>'bail|max:25|alpha']);
        // $validated = $request->validated();
        // dd($validated);
        //

        $jenis = $request->input('tambahjenis');
        // dd($request->input());
        $type = new Type;
        $type->nama = $jenis;
        $type->save();
        $messages = [
            'pesan' => "data berhasil di input",
            'code' => 200
        ];
        return $type;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $ubah = Type::where('id', $id)->first();
        // dd($id);
        $jenis = $request->input('ubahjenis');
        $ubah->nama = $jenis;
        $ubah->save();
        $messages = [
            'pesan' => "data berhasil di ubah",
            'code' => 200
        ];
        return ($messages);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd('hi');
        $jenis = Type::where('id',$id)->withTrashed()->first();
        $jenis->delete();
        $messages = [
            'pesan' => "data berhasil di hapus",
            'code' => 200
        ];
        return ($messages);
    }

    public function coba(Request $request)
    {

        $queryStringSearch = $request->query('search');
        $queryStringDraw = $request->query('draw');
        $queryStringStart = $request->query('start');
        $queryStringLength = $request->query('length');

        $search = $queryStringSearch['value'];

        //membuat variabel dengan menggunakan database atau inisialisasi
        $type = Type::class;

        //menentukan totalrecord yang akan digunakan pada datatable
        $typeTotalRecords = $type::withTrashed()->count();

        //fugsi search
        $typeRecords = $type::when($search, function($src, $search) {
            return $src->where('nama', 'like', "%$search%");
        })->withTrashed();

        //menentukan variabel untuk filterrecord datatable
        $typeFilteredRecords = $typeRecords->count();
        //menentukan limit dari pagination
        $types = $typeRecords->offset($queryStringStart)
        ->limit($queryStringLength)
        //jumlah ticket:
        ->withCount('tickets')
        ->orderBy('id', 'desc')
        // ->withTrashed()
        ->get();

        //masukan urutan nomor:
        $arrayJenis = $types->toArray();
        $i = $queryStringStart;
        $ajens = [];
        foreach($arrayJenis as $ajen) {
            $ajen['no'] = ++$i;
            $ajens[] = $ajen;
        }


        //nilai untuk datatable
        $kudata = [
            'draw' => $queryStringDraw,
            // 'recordsTotal' => $jenisCount,
            'recordsTotal' => $typeTotalRecords,
            // 'recordsFiltered' => $filteredRecord,
            'recordsFiltered' => $typeFilteredRecords,
            "data" => $ajens


        ];
        // dd($kudata);
        return $kudata;
    }



    public function ubah(Request $request,$id)
    {
        $ubah = Type::find($id);
        // dd($id);
        $jenis = $request->input('ubahjenis');
        $ubah->nama = $jenis;
        $ubah->save();
        $messages = [
            'pesan' => "data berhasil di ubah",
            'code' => 200
        ];
        return($ubah);
        return ($messages);

    }

    public function restore($id){

        $jenis = Type::where('id',$id)->withTrashed()->first();
        $jenis->restore();
        $messages = [
            'pesan' => "data berhasil di hapus",
            'code' => 200
        ];
        return ($messages);
    }

    public function realdelete($id){

        $jenis = Type::where('id',$id)->withTrashed()->first();
        $jenis->forceDelete();
        $messages = [
            'pesan' => "data berhasil di hapus",
            'code' => 200
        ];
        return ($messages);
    }





}
