<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Phone;

class UserController extends Controller
{
    public function addPhone()
    {
        $user = User::with('phone')->first();

        $phone = new Phone();
        $phone->phone = '082246205201';

        $user->phone()->save($phone);
    }
}
