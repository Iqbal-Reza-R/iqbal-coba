$("document").ready(function() {


    //tampilan tabel
    $.ajax({
        method: "GET",
        url: "/jenis/muncul"
    }).done(function(responses) {
        tampildatatable(responses);
        $("#jenis-tabel").DataTable();
    })

    //coba onclick
    $("input[name=tambahjenis]").click(function() {
        $("#content").css("background-color", "white");
    });

    $("input[name=tambahjenis]").blur(function() {
        $("#content").css("background-color", "white");
    });

    //token csrf
    $.ajaxSetup({
        headers: {
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")
        }
    });

    //tambah data
    $("#tambah").submit(function(event) {
        event.preventDefault();
        $(".tambah").attr("disabled", "disabled");

        $.ajax({
            method: "POST",
            url: "/jenis/tambah",
            data: $("#tambah").serialize()
        })
            .done(function(data) {
                $("#pesan").append(data.pesan);
                $("#jenis-tabel")
                    .DataTable()
                    .destroy();
                $("#jenis-tabel tbody").empty();
                $("input[name=tambahjenis]").val("");
            })
            .always(function() {
                setTimeout(function() {
                    $("#pesan").empty();
                    $(".tambah").attr("disabled", false);
                }, 2000);

                $.ajax({
                    method: "GET",
                    url: "/jenis/muncul"
                }).done(function(responses) {
                    tampildatatable(responses);

                }).always(function() {
                    $("#jenis-tabel").DataTable();
                });

                // $.ajax({
                //     method: "GET",
                //     url: "/jenis/muncul"
                // }).done(function(responses) {
                //     $.each(responses, function(index, response) {
                //         $('#jenis-tabel tbody').append(
                //             `<tr>
                //                 <td>${index + 1}</td>
                //                 <td class="namadata" data-nama=${response.nama}>${response.nama}</td>
                //                 <td>
                //                     <a href="#" class="edit badge badge-success" data-id="${response.id}"> EDIT </a>
                //                     <a href="/jenis/hapus/${ response.id }" class="delete badge badge-danger" onclick="return confirm('Hapus Data??');"> HAPUS </a>
                //                 </td>
                //             </tr>`
                //             )
                //     })
                // })
            });
    });

    // $('#jenis-tabel tbody').on('change', function() {
    //     $.ajax({
    //         method: "GET",
    //         url: "/jenis/muncul"
    //     }).done(function(responses) {
    //        tampildatatable(responses)
    //        $('#jenis-tabel').DataTable()
    //     })
    // })

    //menentukan action dan method dari form modal
    $(document).on("click", "#jenis-tabel a.edit", function(event) {
        let kucing = $(event.target)
            .parent()
            .siblings(".namadata")
            .data("nama");
        let kucing2 = $(event.target).data("id");
        $("input[name=ubahjenis]").val(kucing);
        $("#formModal").attr("action", "/jenis/ubah/" + kucing2);
        $("#exampleModal").modal("show");
    });

    //Update data dari modal
    $("#formModal").submit(function(event) {
        event.preventDefault();

        $.ajax({
            method: $("#formModal").attr("method"),
            url: $("#formModal").attr("action"),
            data: $("#formModal").serialize()
        })
            .done(function(data) {
                $("#pesan").append(data.pesan);
                $("#jenis-table").DataTable().destroy();
                $("#jenis-tabel tbody").empty();
            })
            .always(function() {
                setTimeout(function() {
                    $("#exampleModal").modal("hide");
                }, 100);
                setTimeout(function() {
                    $("#pesan").empty();
                }, 2000);
                $.ajax({
                    method: "GET",
                    url: "/jenis/muncul"
                }).done(function(responses) {
                    tampildatatable(responses);
                }).always(function() {
                    $("#jenis-tabel").DataTable();
                });

                // $.ajax({
                //     method: "GET",
                //     url: "/jenis/muncul"
                // }).done(function(responses) {
                //     $.each(responses, function(index, response) {
                //         $('#jenis-tabel tbody').append(
                //             `<tr>
                //                 <td>${index + 1}</td>
                //                 <td class="namadata" data-nama=${response.nama}>${response.nama}</td>
                //                 <td>
                //                     <a href="#" class="edit badge badge-success" data-id="${response.id}"> EDIT </a>
                //                     <a href="/jenis/hapus/${ response.id }" class="delete badge badge-danger" onclick="return confirm('Hapus Data??');"> HAPUS </a>
                //                 </td>
                //             </tr>`
                //             )
                //     })
                // })
            });
    });
});


    //isi tabel
    // function tampildatatable(responses) {
    //     $.each(responses, function(index, response) {
    //         $("#jenis-tabel tbody").append(
    //             `<tr>
    //                 <td>${index + 1}</td>
    //                 <td class="namadata" data-nama=${response.nama}>${
    //                 response.nama
    //             }</td>
    //                 <td>
    //                     <a href="#" class="edit badge badge-success" data-id="${
    //                         response.id
    //                     }"> EDIT </a>
    //                     <a href="/jenis/hapus/${
    //                         response.id
    //                     }" class="delete badge badge-danger" onclick="return confirm('Hapus Data??');"> HAPUS </a>
    //                 </td>
    //             </tr>`
    //         );
    //     });
    // }


        function tampildatatable(responses) {
        $.each(responses, function(index, response) {
            $("#jenis-tabel tbody").append(
                `<tr>
                    <td>${index + 1}</td>
                    <td class="namadata" data-nama=${response.nama}>${
                    response.nama
                }</td>
                    <td>
                        <a href="#" class="edit badge badge-success" data-id="${
                            response.id
                        }"> EDIT </a>
                        <a href="/jenis/hapus/${
                            response.id
                        }" class="delete badge badge-danger" onclick="return confirm('Hapus Data??');"> HAPUS </a>
                    </td>
                </tr>`
            );
        });
    }
